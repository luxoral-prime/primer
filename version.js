const { version } = require("./package.json");
const fs = require("fs");
const path = require("path");

const file = path.resolve(__dirname, "source", "Primer.ts");
const source = fs.readFileSync(file).toString();

fs.writeFileSync(
    file,
    source.replace(
        /export const version = ".*";/,
        `export const version = "${version}";`
    )
);
