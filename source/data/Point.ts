/** A point in 2d space */
export interface Point {
    /** The x position in space */
    x: number;
    /** The y position in space */
    y: number;
}
