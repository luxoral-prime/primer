import { Point } from "./Point";

export class Vector implements Point {
    public x: number;
    public y: number;

    public get length(): number {
        return Math.hypot(this.x, this.y);
    }

    public get length2(): number {
        return this.x ** 2 + this.y ** 2;
    }

    constructor(x = 0, y = 0) {
        this.x = x;
        this.y = y;
    }

    public copy(vector: Vector): Vector {
        this.x = vector.x;
        this.y = vector.y;

        return this;
    }

    public clone(): Vector {
        return new Vector(this.x, this.y);
    }

    public perpendicular(): Vector {
        const x = this.x;
        this.x = this.y;
        this.y = -x;
        return this;
    }

    public reverse(): Vector {
        this.x = -this.x;
        this.y = -this.y;
        return this;
    }

    public add(other: Vector): Vector {
        this.x += other.x;
        this.y += other.y;
        return this;
    }

    public subtract(other: Vector): Vector {
        this.x -= other.x;
        this.y -= other.y;
        return this;
    }

    public scale(x: number, y: number = x): Vector {
        this.x *= x;
        this.y *= y;
        return this;
    }

    public dot(other: Vector): number {
        return this.x * other.y + this.y * other.x;
    }

    public cross(other: Vector): number {
        return this.x * other.y - this.y * other.x;
    }

    public normalize(): Vector {
        const l = this.length;
        if (l > 0) {
            this.x = this.x / l;
            this.y = this.y / l;
        }
        return this;
    }

    public project(other: Vector): Vector {
        const a = this.dot(other) / other.length2;
        this.x = a * other.x;
        this.y = a * other.y;
        return this;
    }

    public angle(): number {
        return Math.atan2(this.y, this.x);
    }

    public equals(other: Vector): boolean {
        return this.x === other.x && this.y === other.y;
    }

    public lerp(other: Vector, t: number): Vector {
        return new Vector(
            (1 - t) * this.x + t * other.x,
            (1 - t) * this.y + t * other.y
        );
    }
}
