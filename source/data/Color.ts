/** Represents a color on the RGB color spectrum */
export interface RGBColor {
    /** The red value of the color */
    r: number;
    /** The green value of the color */
    g: number;
    /** The blue value of the color */
    b: number;
}

/** Represents a color on the RGBA color spectrum */
export interface RGBAColor extends RGBColor {
    /** The alpha value of the color */
    a: number;
}

export class Color implements RGBAColor {
    public r: number;
    public g: number;
    public b: number;
    public a: number;

    constructor(r = 0, g = 0, b = 0, a = 1) {
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;
    }

    /** Returns a css color rgb value */
    public get rgb(): string {
        return `rgb(${this.r}, ${this.g},  ${this.b})`;
    }

    /** Returns a css color rgba value */
    public get rgba(): string {
        return `rgba(${this.r}, ${this.g},  ${this.b},  ${this.a})`;
    }
}
