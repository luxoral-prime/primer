import { Point } from "../Primer";

export interface TouchPoint extends Point {
    id: number;
}
