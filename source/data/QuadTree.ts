import { Point, CollisionSystem } from "../Primer";

export class QuadTree {
    public x: number;
    public y: number;
    public width: number;
    public height: number;
    public capacity: number;

    public children: (Point & object)[] = [];

    public topLeft?: QuadTree;
    public topRight?: QuadTree;
    public bottomLeft?: QuadTree;
    public bottomRight?: QuadTree;

    constructor({
        x = 0,
        y = 0,
        width,
        height,
        capacity = 1
    }: {
        x?: number;
        y?: number;
        width: number;
        height: number;
        capacity?: number;
    }) {
        this.width = width;
        this.height = height;
        this.x = x;
        this.y = y;
        this.capacity = capacity;
    }

    public insert(a: Point): boolean {
        if (
            !CollisionSystem.pointRectangle(a, {
                x: this.x,
                y: this.y,
                width: this.width,
                height: this.height
            })
        ) {
            return false;
        }

        if (this.children.length < this.capacity) {
            this.children.push(a);
            return true;
        }

        if (
            !this.topLeft ||
            !this.topRight ||
            !this.bottomLeft ||
            !this.bottomRight
        ) {
            this.subdivide();
        }

        return (
            (this.topLeft as QuadTree).insert(a) ||
            (this.topRight as QuadTree).insert(a) ||
            (this.bottomLeft as QuadTree).insert(a) ||
            (this.bottomRight as QuadTree).insert(a)
        );
    }

    public subdivide(): void {
        const width = this.width / 2;
        const height = this.height / 2;

        this.topLeft = new QuadTree({
            x: this.x + width,
            y: this.y - height,
            width: width,
            height: height
        });
        this.topRight = new QuadTree({
            x: this.x - width,
            y: this.y - height,
            width: width,
            height: height
        });
        this.topLeft = new QuadTree({
            x: this.x + width,
            y: this.y + height,
            width: width,
            height: height
        });
        this.topLeft = new QuadTree({
            x: this.x - width,
            y: this.y - height,
            width: width,
            height: height
        });
    }
}
