export const version = "0.1.15";

((): void =>
    console.log(`
 ___     _                | A default component system
| _ \\_ _(_)_ __  ___ _ _  | library for PrimeEngine.
|  _/ '_| | '  \\/ -_) '_| | Made by @Snabel#5886
|_| |_| |_|_|_|_\\___|_|   | Version ${version}
`))();

export * from "./components/Arc";
export * from "./components/Circle";
export * from "./components/Collidable";
export * from "./components/JoystickInput";
export * from "./components/KeyboardInput";
export * from "./components/MouseInput";
export * from "./components/Polygon";
export * from "./components/Polyline";
export * from "./components/Rectangle";
export * from "./components/Renderable";
export * from "./components/RoundedRectangle";
export * from "./components/Sprite";
export * from "./components/Text";
export * from "./components/TouchInput";
export * from "./components/Transform";

export * from "./data/Color";
export * from "./data/CornerRadius";
export * from "./data/Point";
export * from "./data/QuadTree";
export * from "./data/TouchPoint";
export * from "./data/Vector";

export * from "./systems/CanvasRenderingSystem";
export * from "./systems/CollisionSystem";
export * from "./systems/JoystickInputSystem";
export * from "./systems/KeyboardInputSystem";
export * from "./systems/MouseInputSystem";
export * from "./systems/TouchInputSystem";
