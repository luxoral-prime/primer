import { Entity, EntityQueue, Scene, System } from "PrimeEngine";
import {
    Arc,
    Color,
    JoystickInput,
    MouseInput,
    Point,
    Renderable,
    TouchInput,
    Transform
} from "../Primer";

export interface JoystickInputOptions {
    baseRadius: number;
    baseColor: Color;
    headRadius: number;
    headColor: Color;
    mouse: boolean;
}

export const defaultJoystickInputOptions: JoystickInputOptions = {
    baseRadius: 100,
    baseColor: new Color(255, 255, 255, 0.5),
    headRadius: 50,
    headColor: new Color(255, 255, 255, 0.5),
    mouse: !("ontouchstart" in window || navigator.maxTouchPoints)
};

export class JoystickInputSystem extends System {
    private options: JoystickInputOptions;

    private down = false;
    private touchId: number | undefined;

    private angle = 0;
    private distance = 0;

    private base: Entity;
    private head: Entity;
    private input: Entity;

    public readonly queries = {
        "": [JoystickInput]
    };

    constructor(
        scene: Scene,
        options: JoystickInputOptions = defaultJoystickInputOptions
    ) {
        super();

        this.options = options;

        const baseEntity = new Entity("_JoystickBase", [
            Transform,
            Renderable,
            Arc
        ]);

        baseEntity.components.get(Renderable).visible = false;
        baseEntity.components.get(Renderable).priority = -1;
        baseEntity.components.get(Renderable).fill = {
            enabled: true,
            style: options.baseColor.rgba
        };
        baseEntity.components.get(Arc).radius = 2 * Math.PI;
        baseEntity.components.get(Arc).radius = options.baseRadius;

        const headEntity = new Entity("_JoystickHead", [
            Transform,
            Renderable,
            Arc
        ]);

        headEntity.components.get(Renderable).visible = false;
        headEntity.components.get(Renderable).priority = -2;
        headEntity.components.get(Renderable).fill = {
            enabled: true,
            style: options.headColor.rgba
        };
        headEntity.components.get(Arc).radius = 2 * Math.PI;
        headEntity.components.get(Arc).radius = options.headRadius;

        const inputEntity = new Entity("_JoystickInput", [
            options.mouse ? MouseInput : TouchInput
        ]);

        scene.entities.add((this.base = baseEntity));
        scene.entities.add((this.head = headEntity));
        scene.entities.add((this.input = inputEntity));
    }

    public update(entities: EntityQueue, delta: number): void {
        this.updateJoystick();

        for (const entity of entities[""]) {
            entity.components.get(JoystickInput).angle = this.angle;
            entity.components.get(JoystickInput).distance = this.distance;
        }
    }

    private updateJoystick(): void {
        let inputDown = false;
        let inputPosition: Point = { x: 0, y: 0 };

        if (this.options.mouse) {
            const mouse = this.input.components.get(MouseInput);

            inputDown = mouse.left;

            inputPosition = { ...mouse.position };
        } else {
            const touch = this.input.components.get(TouchInput);

            if (!this.down && !this.touchId && touch.touches.length > 0) {
                inputDown = true;
                this.touchId = touch.touches[0].id;
            }

            for (const point of touch.touches) {
                if (point.id === this.touchId) {
                    inputPosition.x = point.x;
                    inputPosition.y = point.y;
                    inputDown = true;
                }
            }
        }

        const baseRenderable = this.base.components.get(Renderable);
        const baseTransform = this.base.components.get(Transform);
        const headRenderable = this.head.components.get(Renderable);
        const headTransform = this.head.components.get(Transform);

        if (!this.down && inputDown) {
            this.down = true;
            baseRenderable.visible = true;
            headRenderable.visible = true;

            baseTransform.position = { ...inputPosition };
            headTransform.position = { ...inputPosition };
        } else if (this.down && !inputDown) {
            this.down = false;
            baseRenderable.visible = false;
            headRenderable.visible = false;
        }

        if (!(this.down && inputDown)) return;

        this.angle = Math.atan2(
            inputPosition.x - baseTransform.position.x,
            inputPosition.y - baseTransform.position.y
        );

        this.distance = Math.min(
            Math.hypot(
                baseTransform.position.x - inputPosition.x,
                baseTransform.position.y - inputPosition.y
            ),
            this.options.baseRadius - this.options.headRadius
        );

        headTransform.position.x =
            baseTransform.position.x + this.distance * Math.sin(this.angle);
        headTransform.position.y =
            baseTransform.position.y + this.distance * Math.cos(this.angle);
    }
}
