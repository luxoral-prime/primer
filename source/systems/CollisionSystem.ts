import { EntityQueue, System, Entity } from "PrimeEngine";
import { Collidable, Point } from "../Primer";

import { Polygon, Polyline, Rectangle, Transform } from "../Primer";
import { RoundedRectangle } from "../components/RoundedRectangle";

export class CollisionSystem extends System {
    public readonly queries = {
        Collidable: [Collidable, Transform]
    };

    static pointPoint(a: Point, b: Point): boolean {
        return a.x === b.x && a.y === b.y;
    }

    static pointCircle(a: Point, b: { radius: number } & Point): boolean {
        return (a.x - b.x) ** 2 + (a.y - b.y) ** 2 <= b.radius ** 2;
    }

    static pointLine(a: Point, b: [Point, Point]): boolean {
        return (
            (a.x - b[0].x) * (b[1].x - b[0].x) -
                (a.y - b[0].y) * (b[1].y - b[0].y) ===
            0
        );
    }

    static circleCircle(
        a: { radius: number } & Point,
        b: { radius: number } & Point
    ): boolean {
        return (
            (a.x - b.x) ** 2 + (a.y - b.y) ** 2 <= (a.radius + b.radius) ** 2
        );
    }

    static circleRectangle(
        a: { radius: number } & Point,
        b: { width: number; height: number } & Point
    ): boolean {
        return (
            (a.x - Math.max(b.x, Math.min(a.x, b.x + b.width))) ** 2 +
                (a.y - Math.max(b.y, Math.min(a.y, b.y + b.height))) ** 2 <=
            a.radius ** 2
        );
    }

    static pointRectangle(
        a: Point,
        b: { width: number; height: number } & Point
    ): boolean {
        return (
            a.x <= b.x + b.width &&
            a.x >= b.x &&
            a.y <= b.y + b.height &&
            a.y >= b.y
        );
    }

    static rectangleRectangle(
        a: { width: number; height: number } & Point,
        b: { width: number; height: number } & Point
    ): boolean {
        return (
            a.x <= b.x + b.width &&
            a.x + a.width >= b.x &&
            a.y <= b.y + b.height &&
            a.y + a.height >= b.y
        );
    }

    static lineLine(a: [Point, Point], b: [Point, Point]): boolean {
        const A =
            ((b[1].x - b[0].x) * (a[0].y - b[0].y) -
                (b[1].y - b[0].y) * (a[0].x - b[0].x)) /
            ((b[1].y - b[0].y) * (a[1].x - a[0].x) -
                (b[1].x - b[0].x) * (a[1].y - a[0].y));

        const B =
            ((a[1].x - a[0].x) * (a[0].y - b[0].y) -
                (a[1].y - a[0].y) * (a[0].x - b[0].x)) /
            ((b[1].y - b[0].y) * (a[1].x - a[0].x) -
                (b[1].x - b[0].x) * (a[1].y - a[0].y));

        return A >= 0 && A <= 1 && B >= 0 && B <= 1;
    }

    static lineLineIntersection(
        a: [Point, Point],
        b: [Point, Point]
    ): Point | undefined {
        const A =
            ((b[1].x - b[0].x) * (a[0].y - b[0].y) -
                (b[1].y - b[0].y) * (a[0].x - b[0].x)) /
            ((b[1].y - b[0].y) * (a[1].x - a[0].x) -
                (b[1].x - b[0].x) * (a[1].y - a[0].y));

        const B =
            ((a[1].x - a[0].x) * (a[0].y - b[0].y) -
                (a[1].y - a[0].y) * (a[0].x - b[0].x)) /
            ((b[1].y - b[0].y) * (a[1].x - a[0].x) -
                (b[1].x - b[0].x) * (a[1].y - a[0].y));

        if (A >= 0 && A <= 1 && B >= 0 && B <= 1) {
            return {
                x: a[0].x + A * (a[1].x - a[0].x),
                y: a[0].y + A * (a[1].y - a[0].y)
            };
        } else {
            return;
        }
    }

    // https://stackoverflow.com/questions/10962379/how-to-check-intersection-between-2-rotated-rectangles
    // Basically SAT
    static polygonPolygon(a: Point[], b: Point[]): boolean {
        for (const polygon of [a, b]) {
            for (let i0 = 0; i0 < polygon.length; i0++) {
                const i1 = (i0 + 1) % polygon.length;
                const p0 = polygon[i0];
                const p1 = polygon[i1];
                const nX = p1.y - p0.y;
                const nY = p1.x - p0.x;

                let minA = Number.POSITIVE_INFINITY;
                let maxA = Number.NEGATIVE_INFINITY;
                for (let j = 0; j < a.length; j++) {
                    const proj = nX * a[j].x + nY * a[j].y;

                    if (proj < minA) minA = proj;
                    if (proj > maxA) maxA = proj;
                }

                let minB = Number.POSITIVE_INFINITY;
                let maxB = Number.NEGATIVE_INFINITY;
                for (let j = 0; j < b.length; j++) {
                    const proj = nX * b[j].x + nY * b[j].y;

                    if (proj < minB) minB = proj;
                    if (proj > maxB) maxB = proj;
                }

                if (maxA < minB || maxB < minA) return false;
            }
        }

        return true;
    }

    public update(entities: EntityQueue, delta: number): void {
        for (const entityA of entities.Collidable) {
            const collidableA = entityA.components.get(
                Collidable
            ) as Collidable;
            const transformA = entityA.components.get(Transform) as Transform;

            collidableA.collisions = [];

            for (const entityB of entities.Collidable) {
                if (entityA.name === entityB.name) continue;

                const collidableB = entityB.components.get(Collidable);
                const transformB = entityB.components.get(Transform);

                const boundaryA = {
                    width: collidableA.boundary.width,
                    height: collidableA.boundary.height,
                    x: collidableA.boundary.x + transformA.position.x,
                    y: collidableA.boundary.y + transformA.position.y
                };

                const boundaryB = {
                    width: collidableB.boundary.width,
                    height: collidableB.boundary.height,
                    x: collidableB.boundary.x + transformB.position.x,
                    y: collidableB.boundary.y + transformB.position.y
                };

                if (CollisionSystem.rectangleRectangle(boundaryA, boundaryB)) {
                    if (entityA.components.has(Rectangle)) {
                        const rectangleA = entityA.components.get(
                            Rectangle
                        ) as Rectangle;

                        // Rectangle Rectangle collision
                        if (entityB.components.has(Rectangle)) {
                            const rectangleB = entityB.components.get(
                                Rectangle
                            ) as Rectangle;

                            if (
                                CollisionSystem.polygonPolygon(
                                    rectangleA.toVertices(),
                                    rectangleB.toVertices()
                                )
                            ) {
                                collidableA.collisions.push(entityB);
                            }
                        }

                        // Rectangle RoundedRectangle collision
                        if (entityB.components.has(RoundedRectangle)) {
                            const roundedRectangleB = entityB.components.get(
                                RoundedRectangle
                            ) as RoundedRectangle;

                            if (
                                CollisionSystem.polygonPolygon(
                                    rectangleA.toVertices(),
                                    roundedRectangleB.toVertices()
                                )
                            ) {
                                collidableA.collisions.push(entityB);
                            }
                        }

                        // Rectangle Polygon collision
                        if (entityB.components.has(Polygon)) {
                            const polygonB = entityB.components.get(
                                Polygon
                            ) as Polygon;

                            if (
                                CollisionSystem.polygonPolygon(
                                    rectangleA.toVertices(),
                                    polygonB.vertices
                                )
                            ) {
                                collidableA.collisions.push(entityB);
                            }
                        }
                    }

                    if (entityA.components.has(RoundedRectangle)) {
                        const roundedRectangleA = entityA.components.get(
                            RoundedRectangle
                        ) as RoundedRectangle;

                        // RoundedRectangle Rectangle
                        if (entityB.components.has(Rectangle)) {
                            const rectangleB = entityB.components.get(
                                Rectangle
                            ) as Rectangle;

                            if (
                                CollisionSystem.polygonPolygon(
                                    roundedRectangleA.toVertices(),
                                    rectangleB.toVertices()
                                )
                            ) {
                                collidableA.collisions.push(entityB);
                            }
                        }

                        // RoundedRectangle RoundedRectangle
                        if (entityB.components.has(RoundedRectangle)) {
                            const roundedRectangleB = entityB.components.get(
                                RoundedRectangle
                            ) as RoundedRectangle;

                            if (
                                CollisionSystem.polygonPolygon(
                                    roundedRectangleA.toVertices(),
                                    roundedRectangleB.toVertices()
                                )
                            ) {
                                collidableA.collisions.push(entityB);
                            }
                        }

                        // RoundedRectangle Polygon
                        if (entityB.components.has(Polygon)) {
                            const polygonB = entityB.components.get(
                                Polygon
                            ) as Polygon;

                            if (
                                CollisionSystem.polygonPolygon(
                                    roundedRectangleA.toVertices(),
                                    polygonB.vertices
                                )
                            ) {
                                collidableA.collisions.push(entityB);
                            }
                        }
                    }
                }
            }

            if (collidableA.solid) {
                if (collidableA.collisions.length >= 1) {
                    if (
                        collidableA.collisions.find(
                            (entity: Entity) =>
                                entity.components.get(Collidable).solid
                        )
                    )
                        transformA.position = {
                            x: collidableA.previous.x,
                            y: collidableA.previous.y
                        };

                    transformA.rotation = collidableA.previous.rotation;
                } else {
                    collidableA.previous = {
                        x: transformA.position.x,
                        y: transformA.position.y,
                        xrotation: transformA.rotation
                    };
                }
            }
        }
    }
}
