import { EntityQueue, System, Entity } from "PrimeEngine";
import { KeyboardInput } from "../Primer";

export class KeyboardInputSystem extends System {
    public pressed: Set<string>;
    public justPressed: Set<string>;
    public justReleased: Set<string>;

    public queries = {
        "": [KeyboardInput]
    };

    constructor() {
        super();

        this.pressed = new Set();
        this.justPressed = new Set();
        this.justReleased = new Set();

        window.addEventListener("keydown", this.onKeyDown.bind(this));
        window.addEventListener("keyup", this.onKeyUp.bind(this));
    }

    private onKeyDown(event: KeyboardEvent): void {
        if (event.defaultPrevented) return;

        this.justPressed.add(event.key);
        this.pressed.add(event.key);

        event.preventDefault();
    }

    private onKeyUp(event: KeyboardEvent): void {
        if (event.defaultPrevented) return;

        this.justReleased.add(event.key);
        this.pressed.delete(event.key);

        event.preventDefault();
    }

    public update(queue: EntityQueue, delta: number): void {
        const entities = queue[""] as Entity[];

        for (const entity of entities) {
            const component = entity.components.get(KeyboardInput);

            component.pressed = this.pressed;
            component.justPressed = new Set(this.justPressed);
            component.justReleased = new Set(this.justReleased);
        }

        this.justPressed.clear();
        this.justReleased.clear();
    }
}
