import { EntityQueue, System } from "PrimeEngine";
import { TouchInput, TouchPoint } from "../Primer";

export class TouchInputSystem extends System {
    private element: HTMLElement;

    private touches: TouchPoint[];
    private changed: TouchPoint[];

    public readonly queries = {
        "": [TouchInput]
    };

    constructor(element: HTMLElement = document.body) {
        super();

        this.element = element;

        this.touches = [];
        this.changed = [];

        this.element.addEventListener(
            "touchstart",
            this.onTouchEvent.bind(this)
        );
        this.element.addEventListener("touchend", this.onTouchEvent.bind(this));
        this.element.addEventListener(
            "touchmove",
            this.onTouchEvent.bind(this)
        );
    }

    private onTouchEvent(event: TouchEvent): void {
        this.touches = [];

        for (const touch of event.touches) {
            this.touches[touch.identifier] = {
                x: touch.clientX - this.element.offsetLeft,
                y: touch.clientY - this.element.offsetTop,
                id: touch.identifier
            };
        }

        for (const touch of event.changedTouches) {
            this.changed[touch.identifier] = {
                x: touch.clientX - this.element.offsetLeft,
                y: touch.clientY - this.element.offsetTop,
                id: touch.identifier
            };
        }
    }

    public update(entities: EntityQueue, delta: number): void {
        for (const entity of entities[""]) {
            entity.components.get(TouchInput).touches = this.touches;
            entity.components.get(TouchInput).changed = this.changed;
        }
    }
}
