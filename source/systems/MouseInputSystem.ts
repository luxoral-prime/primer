import { Entity, EntityQueue, System } from "PrimeEngine";
import { MouseInput, Point } from "../Primer";

export class MouseInputSystem extends System {
    private element: HTMLElement;
    private position: Point;
    private left: boolean;
    private middle: boolean;
    private right: boolean;

    public queries = {
        "": [MouseInput]
    };

    constructor(element: HTMLElement = document.body) {
        super();

        this.element = element;

        this.position = { x: 0, y: 0 };
        this.left = false;
        this.middle = false;
        this.right = false;

        this.element.addEventListener("mousemove", this.onMouseMove.bind(this));
        this.element.addEventListener("mousedown", this.onMouseDown.bind(this));
        this.element.addEventListener("mouseup", this.onMouseUp.bind(this));
    }

    private onMouseMove(event: MouseEvent): void {
        this.position.x = event.clientX - this.element.offsetLeft;
        this.position.y = event.clientY - this.element.offsetTop;
    }

    private onMouseDown(event: MouseEvent): void {
        switch (event.button) {
            case 0:
                this.left = true;
                break;
            case 1:
                this.middle = true;
                break;
            case 2:
                this.right = true;
                break;
        }
    }

    private onMouseUp(event: MouseEvent): void {
        switch (event.button) {
            case 0:
                this.left = false;
                break;
            case 1:
                this.middle = false;
                break;
            case 2:
                this.right = false;
                break;
        }
    }

    public update(queue: EntityQueue, delta: number): void {
        const entities = queue[""] as Entity[];

        for (const entity of entities) {
            const component = entity.components.get(MouseInput);

            component.position = this.position;
            component.left = this.left;
            component.middle = this.middle;
            component.right = this.right;
        }
    }
}
