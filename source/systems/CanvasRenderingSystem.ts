import { Entity, EntityQueue, System } from "PrimeEngine";
import {
    Arc,
    Polygon,
    Polyline,
    Rectangle,
    Renderable,
    RoundedRectangle,
    Sprite,
    Text,
    Transform,
    Circle
} from "../Primer";

export interface CanvasRenderingSystemOptions {
    clearColor: undefined | CanvasFillStrokeStyles["fillStyle"];
}

export const defaultCanvasRenderingSystemOptions: CanvasRenderingSystemOptions = {
    clearColor: undefined
};

export class CanvasRenderingSystem extends System {
    private context: CanvasRenderingContext2D;
    private options: CanvasRenderingSystemOptions;

    public readonly queries = {
        RenderableArc: [Renderable, Transform, Arc],
        RenderableCircle: [Renderable, Transform, Circle],
        RenderablePolygon: [Renderable, Transform, Polygon],
        RenderablePolyline: [Renderable, Transform, Polyline],
        RenderableRectangle: [Renderable, Transform, Rectangle],
        RenderableRoundedRectangle: [Renderable, RoundedRectangle, Transform],
        RenderableSprite: [Renderable, Transform, Sprite],
        RenderableText: [Renderable, Transform, Text]
    };

    constructor(
        canvas: HTMLCanvasElement,
        options: CanvasRenderingSystemOptions = defaultCanvasRenderingSystemOptions
    ) {
        super();

        const context = canvas.getContext("2d");
        if (!context)
            throw new Error("Could not get canvas rendering context 2d");
        this.context = context;

        this.options = options;
    }

    private clear(): void {
        if (this.options.clearColor) {
            this.context.fillStyle = this.options.clearColor;
            this.context.fillRect(
                0,
                0,
                this.context.canvas.width,
                this.context.canvas.height
            );
        } else {
            this.context.clearRect(
                0,
                0,
                this.context.canvas.width,
                this.context.canvas.height
            );
        }
    }

    private drawArc(arc: Arc): void {
        this.context.arc(
            0,
            0,
            arc.radius,
            arc.startAngle,
            arc.endAngle,
            !arc.clockwise
        );
    }

    private drawCircle(circle: Circle): void {
        this.context.arc(0, 0, circle.radius, 0, 2 * Math.PI);
    }

    private drawPolygon(polygon: Polygon): void {
        const vertices = [...polygon.vertices];
        if (vertices.length < 2) return;

        const start = vertices.shift();
        if (!start) return;

        this.context.moveTo(start.x, start.y);

        for (const vertex of vertices) {
            this.context.lineTo(vertex.x, vertex.y);
        }

        this.context.closePath();
    }

    private drawPolyline(polyline: Polyline): void {
        const vertices = [...polyline.vertices];
        if (vertices.length < 2) return;

        const start = vertices.shift();
        if (!start) return;

        this.context.moveTo(start.x, start.y);

        for (const vertex of vertices) {
            this.context.lineTo(vertex.x, vertex.y);
        }
    }

    private drawRectangle(rectangle: Rectangle): void {
        this.context.rect(0, 0, rectangle.width, rectangle.height);
    }

    private drawRoundedRectangle(rectangle: RoundedRectangle): void {
        const radius: [number, number, number, number] =
            typeof rectangle.radius === "number"
                ? [
                      rectangle.radius,
                      rectangle.radius,
                      rectangle.radius,
                      rectangle.radius
                  ]
                : rectangle.radius;

        const { width, height } = rectangle;

        this.context.moveTo(radius[0], 0);
        this.context.lineTo(width - radius[1], 0);
        this.context.quadraticCurveTo(width, 0, width, radius[1]);
        this.context.lineTo(width, height - radius[3]);
        this.context.quadraticCurveTo(width, height, width - radius[3], height);
        this.context.lineTo(radius[2], height);
        this.context.quadraticCurveTo(0, height, 0, height - radius[2]);
        this.context.lineTo(0, radius[0]);
        this.context.quadraticCurveTo(0, 0, radius[0], 0);
    }

    private drawSprite(sprite: Sprite): void {
        if (
            sprite.source.width &&
            sprite.source.height &&
            sprite.source.offset &&
            sprite.height &&
            sprite.width
        ) {
            this.context.drawImage(
                sprite.image,
                sprite.source.offset.x,
                sprite.source.offset.y,
                sprite.source.width,
                sprite.source.height,
                0,
                0,
                sprite.width,
                sprite.height
            );
        } else if (sprite.height && sprite.width) {
            this.context.drawImage(
                sprite.image,
                0,
                0,
                sprite.width,
                sprite.height
            );
        } else {
            this.context.drawImage(sprite.image, 0, 0);
        }
    }

    public update(queue: EntityQueue, delta: number): void {
        this.context.save();
        this.clear();

        const entities = Object.keys(queue)
            .map((query) => {
                return queue[query].map((entity: Entity) => {
                    return {
                        query: query,
                        entity: entity
                    };
                });
            })
            .flat()
            .sort((a, b) => {
                return (
                    b.entity.components.get(Renderable).priority -
                    a.entity.components.get(Renderable).priority
                );
            });

        for (const { query, entity } of entities) {
            const {
                offset,
                fill,
                stroke,
                visible,
                renderWhenOutsideContext,
                text
            } = entity.components.get(Renderable);
            const { position, rotation, scale } = entity.components.get(
                Transform
            );

            if (!visible) continue;

            if (
                !renderWhenOutsideContext &&
                (position.x < 0 ||
                    position.x > this.context.canvas.width ||
                    position.y < 0 ||
                    position.y > this.context.canvas.height)
            )
                continue;

            this.context.save();

            this.context.translate(position.x, position.y);

            this.context.rotate(rotation);

            this.context.scale(scale.width, scale.height);

            this.context.translate(offset.x, offset.y);

            if (fill.enabled) {
                this.context.fillStyle = fill.style;
            }

            if (stroke.enabled) {
                this.context.strokeStyle = stroke.style;
                this.context.lineWidth = stroke.lineWidth;
                this.context.lineCap = stroke.lineCap;
                this.context.lineJoin = stroke.lineJoin;
            }

            this.context.beginPath();

            switch (query) {
                case "RenderableArc":
                    this.drawArc(entity.components.get(Arc));
                    break;
                case "RenderableCircle":
                    this.drawCircle(entity.components.get(Circle));
                    break;
                case "RenderablePolygon":
                    this.drawPolygon(entity.components.get(Polygon));
                    break;
                case "RenderablePolyline":
                    this.drawPolyline(entity.components.get(Polyline));
                    break;
                case "RenderableRectangle":
                    this.drawRectangle(entity.components.get(Rectangle));
                    break;
                case "RenderableRoundedRectangle":
                    this.drawRoundedRectangle(
                        entity.components.get(RoundedRectangle)
                    );
                    break;
                case "RenderableSprite":
                    this.drawSprite(entity.components.get(Sprite));
                    break;
                /* eslint-disable */
                case "RenderableText":
                    this.context.direction = text.direction;
                    this.context.font = text.font;
                    this.context.textAlign = text.textAlign;
                    this.context.textBaseline = text.textBaseline;

                    const lines = entity.components.get(Text).value.split("\n");
                    for (let i = 0; i < lines.length; i++) {
                        if (fill.enabled) {
                            this.context.fillText(
                                lines[i],
                                0,
                                i * text.lineHeight
                            );
                        }
                        if (stroke.enabled) {
                            this.context.strokeText(
                                lines[i],
                                0,
                                i * text.lineHeight
                            );
                        }
                    }
                    break;
                /* eslint-enable */
            }

            if (query !== "RenderableText") {
                if (fill.enabled) {
                    this.context.fill();
                }

                if (stroke.enabled) {
                    this.context.stroke();
                }
            }

            this.context.restore();
        }

        this.context.restore();
    }
}
