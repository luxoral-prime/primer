import { Component } from "PrimeEngine";

export class Arc extends Component {
    public radius = 1;
    public startAngle = 0;
    public endAngle: number = 2 * Math.PI;
    public clockwise = true;
}
