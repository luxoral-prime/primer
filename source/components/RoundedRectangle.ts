import { Component } from "PrimeEngine";
import { CornerRadius } from "../Primer";
import { Point } from "../data/Point";

export class RoundedRectangle extends Component {
    public width = 0;
    public height = 0;
    public radius: CornerRadius = 0;

    public toVertices(): Point[] {
        return [
            { x: 0, y: 0 },
            { x: this.width, y: 0 },
            { x: this.width, y: this.height },
            { x: 0, y: -this.height }
        ];
    }
}
