import { Component } from "PrimeEngine";
import { Point } from "../Primer";

export class Transform extends Component {
    public position: Point = { x: 0, y: 0 };
    public rotation = 0;
    public scale: { width: number; height: number } = {
        width: 1,
        height: 1
    };
}
