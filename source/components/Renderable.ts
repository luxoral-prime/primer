import { Component } from "PrimeEngine";
import { Point } from "../Primer";

export class Renderable extends Component {
    public visible = true;
    public offset: Point = { x: 0, y: 0 };
    public renderWhenOutsideContext = false;
    public priority = 0;

    public fill: {
        enabled: boolean;
        style: CanvasFillStrokeStyles["fillStyle"];
    } = {
        enabled: false,
        style: "#000"
    };
    public stroke: {
        enabled: boolean;
        style: CanvasFillStrokeStyles["strokeStyle"];
        lineWidth: number;
        lineCap: CanvasLineCap;
        lineJoin: CanvasLineJoin;
    } = {
        enabled: false,
        style: "#000",
        lineWidth: 1,
        lineCap: "butt",
        lineJoin: "miter"
    };
    public text: {
        direction: CanvasDirection;
        font: string;
        textAlign: CanvasTextAlign;
        textBaseline: CanvasTextBaseline;
        lineHeight: number;
    } = {
        direction: "inherit",
        font: "16px monospace",
        textAlign: "start",
        textBaseline: "alphabetic",
        lineHeight: 16
    };
}
