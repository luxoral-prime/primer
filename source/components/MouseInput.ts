import { Component } from "PrimeEngine";
import { Point } from "../Primer";

export class MouseInput extends Component {
    public position: Point = { x: 0, y: 0 };
    public left = false;
    public middle = false;
    public right = false;
}
