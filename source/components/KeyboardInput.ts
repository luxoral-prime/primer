import { Component } from "PrimeEngine";

export class KeyboardInput extends Component {
    public pressed: Set<string> = new Set();
    public justPressed: Set<string> = new Set();
    public justReleased: Set<string> = new Set();
}
