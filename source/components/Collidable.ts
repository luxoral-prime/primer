import { Component, Entity } from "PrimeEngine";
import { Point } from "../data/Point";

export class Collidable extends Component {
    public solid = false;

    public previous: Point & { rotation: number } = {
        x: 0,
        y: 0,
        rotation: 0
    };

    public boundary: { width: number; height: number } & Point = {
        width: 1,
        height: 1,
        x: 0,
        y: 0
    };

    public collisions: Entity[] = [];
}
