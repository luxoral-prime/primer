import { Component } from "PrimeEngine";
import { TouchPoint } from "../Primer";

export class TouchInput extends Component {
    touches: TouchPoint[] = [];
    changed: TouchPoint[] = [];
}
