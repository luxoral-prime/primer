import { Component } from "PrimeEngine";
import { Point } from "../Primer";

export class Sprite extends Component {
    public image: CanvasImageSource = new Image();

    // https://mdn.mozillademos.org/files/225/Canvas_drawimage.jpg
    public source: {
        width?: number;
        height?: number;
        offset?: Point;
    } = {};

    public width?: number;
    public height?: number;
}
